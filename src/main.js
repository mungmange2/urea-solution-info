import Vue from 'vue'
import Index from './Index.vue'
import vuetify from './plugins/vuetify'
import axios from 'axios'

Vue.config.productionTip = false
Vue.prototype.$axios = axios

new Vue({
  vuetify,
  render: h => h(Index)
}).$mount('#app')
